# ***Pour exécuter ce projet une fois cloner***
|Date de création| 14/12/2022 |
|---|---|
| Auteur | Serigne |
|Email:| serigne.ndiaye@outlook.com|
|||

- Vous allez installer un environnement virtual python
 
  * `python -m venv env_name`

- Activer l'environnement en appelant la commande dans windows:
  *  `env_name/Scripts/activate`

- Une fois activer, vous aller installer les packages dans le fichier requirements.txt
  * `pip install -r requirements.txt`

||    ***Enjoy your code!***


