

# Projet JobsMapper with OCR & Web Scraping 

| Doc n° | Auteur | Dernière Modification |Version |
|--------|--------|--------|--------|
|0001| Serigne| 12/12/2022|v 0.1|

## I. Objectif du projet :

L'objectif du projet est de faciliter les chercheurs d'emplois de trouver au plus vite l'emploi qui le correspond le mieux.

## Les outils:
On utilisera pour celà la technologie OCR et le web scraping.
   - __Technologie OCR__:
   Optical Character Recognition permet d'extraire du texte à partir d'image, de pdf etc.
   - __WebScraping__: Permet de collecter des données à partir d'un site web. voir [Wikipedia](https://fr.wikipedia.org/wiki/Web_scraping)

### II. Le Web Scraping:

Le 30 avril 2020, [la CNIL](https://archive.wikiwix.com/cache/index2.php?url=https%3A%2F%2Fwww.cnil.fr%2Ffr%2Fla-reutilisation-des-donnees-publiquement-accessibles-en-ligne-des-fins-de-demarchage-commercial#federation=archive.wikiwix.com&tab=url) a publié de nouvelles directives sur le web scraping. Les lignes directrices de la CNIL précisent que les données accessibles au public sont toujours des données personnelles et qu'elles ne peuvent pas être réutilisées à l'insu de la personne à laquelle ces données appartiennent. Voir si c'est [Légal ou pas.](https://archive.wikiwix.com/cache/index2.php?url=https%3A%2F%2Ffinddatalab.com%2Fweb-scraping-legal#federation=archive.wikiwix.com&tab=url)

En décembre 2021, une start-up de la Station F est condamnée pour piratage informatique. À l'aide d'une technique de web scraping, elle a récolté des données de l'annuaire d'une école de commerce parisienne, afin de solliciter les anciens élèves de l'établissement en vue d'alimenter un financement participatif8 La condamnation porte sur la méthode d'accès à la donnée, c'est-à-dire une usurpation d'identité permettant un « accès frauduleux à un système de traitement automatisé de données », et non le web scraping lui-même.

#### II. 1. Notre Objectif:
Notre objectif principal est d'aidé les chercheurs d'emplois à mieux cibler les offres répondants à leurs profils. Ce n'est un objectif commercial.

Vu les artcles sur CNIL et sur d'autres sources sûres, l'utilisation du web scraping à titre personnel pour pouvoir répondre aux offres d'emplois est légal.

#### II. 2. Collection et séléction des données:
Nos cibles principales restent les sites qui publient les offres d'emplois comme [LinkedIn](https://www.linkedin.com/jobs/), [Indeed](https://fr.indeed.com/) etc.
Items recherchés: (titres de l'emplois, la description, et les infos sur le pourvoyeur de l'offre.)
Une fois les données collectées, nous les sauvegardons dans une base de données et les utiliserons pour mapper avec des profils de candidats idéales.

#### II. 3. Les langages et outils pour le web scraping:
Presque tous les langages informatiques peuvent faire du web scraping, mais le plus utilisé reste le langage python avec ces pléthores de librairies.
(**Selenium**: outil principal des testeurs, **Playerwright**: outil concurrent de selenium, **requests**, **httpx**, **selectolax**, **bs4** aka **beautifulShoup** etc.).

Pour l'efficacité et la performance, notre choix porte sur **playerwright**, **httpx** et **selectolax**.

Exemple code playerwright en mode asynchrone:

```
from playwright.async_api import async_playwright
import asyncio

async def main():
    async with async_playwright() as p:
        browser = await p.chromium.launch()
        browser_context = await browser.new_context()
        page = await browser_context.new_page()
        await page.goto("https://www.google.com")

asyncio.run(main())

```

Exemple de code pour selectolax et httpx:

```
import httpx
from selectolax.parser import HTMLParser
from dataclasses import dataclass, asdict
import csv

@dataclass
class Product:
    manufacturer: str
    title: str
    price: str

def get_html(page):
    url = f"https://www.thomann.de/gb/t_models.html?ls=100&pg={page}&setViewMode=list&gk=GIEGTE&cme=false"
    resp = httpx.get(url)
    return HTMLParser(resp.text)



def parse_product(html):
    products = html.css('div.product')
    results = []
    for product in products:
        new_product = Product(
            manufacturer=product.css_first('span.title__manufacturer').text(),
            title=product.css_first('span.title__name').text(),
            price=product.css_first('div.product__price').text().strip(),
        )
        results.append(asdict(new_product))
    return results
    
        # print(new_product)

def to_csv(res):
    with open('results.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=['manufacturer', 'title', 'price'])
        writer.writeheader()
        writer.writerows(res)

def main():
    for x in range(1,4):
        HTML = get_html(x)
        # print(HTML.css_first("title").text())
        res = parse_product(HTML)
        to_csv(res)


if __name__ == "__main__":
    main()
```
