import httpx
from selectolax.parser import HTMLParser
from dataclasses import dataclass, asdict
import csv

@dataclass
class Product:
    manufacturer: str
    title: str
    price: str

def get_html(page):
    url = f"https://www.thomann.de/gb/t_models.html?ls=100&pg={page}&setViewMode=list&gk=GIEGTE&cme=false"
    resp = httpx.get(url)
    return HTMLParser(resp.text)



def parse_product(html):
    products = html.css('div.product')
    results = []
    for product in products:
        new_product = Product(
            manufacturer=product.css_first('span.title__manufacturer').text(),
            title=product.css_first('span.title__name').text(),
            price=product.css_first('div.product__price').text().strip(),
        )
        results.append(asdict(new_product))
    return results
    
        # print(new_product)

def to_csv(res):
    with open('results.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=['manufacturer', 'title', 'price'])
        writer.writeheader()
        writer.writerows(res)

def main():
    for x in range(1,4):
        HTML = get_html(x)
        # print(HTML.css_first("title").text())
        res = parse_product(HTML)
        to_csv(res)


if __name__ == "__main__":
    main()