# KilifaConsulting.

|Date de création|14/12/2004|
|-----------|-----------
|Auteur|Mouhamad FALL|
|Mail|mouhamad.fall.pro@gmail.com|

Voici la page GitLab de la société KilifaConsulting.
Vous trouverez ici tous les progrès réalisés dans les prochains projets informatiques.

## Avancée du projet en cours. 
Le projet en cours porte actuellement sur le WebScraping.

### Tutoriel GitLab.
**Cloner le repot sur VSCODE/ invite de commandes :**                                                                                                             
Pour commencer il va falloir cloner le repository pour pouvoir ensuite modifier nos codes sur Visuals Studios Code directement, pour cela il va falloir appuyer sur le bouton "CLONE" en bleu afin de recupérer le lien HTTPS. Il faudra par la suite aller sur le terminal de VSCode et taper la ligne de code suivante à partir du terminal : "git clone [lien]".

**Quelques commandes Git importantes à connaître. :**  

"Git add ." : Cette ligne de commande va permettre d'enregister l'etât actuel du projet  

"Git commit -m "message"" : Cette commande va permettre d'afficher un message visible par l'enesemble des intervenants sur GitLab

"Git Push" : Cette commande va permettre de directement publier les lignes de code sur GitLab 
